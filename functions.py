# Import necessary modules
try:
    import matplotlib
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    import datetime
    import re

except:
    "Error while loading required modules. Program requires matplotlib, numpy, pandas, datetime and re to run."

def loadData(path=''):
    '''Function for loading in the crash data, returns dataframe or error message,
    takes csv file path as input argument'''
    try:
        data = pd.read_csv(path)
        data['ACCIDENT_DATE'] = pd.to_datetime(data['ACCIDENT_DATE'])  # Converts ACCIDENT_DATE column to datetime
        data['DATETIME'] = pd.to_datetime(data['ACCIDENT_DATE'].dt.strftime('%Y-%m-%d') + ' ' + data['ACCIDENT_TIME']
                                          .str.replace('.', ':'))  # Adds DATETIME column with both date and time as datetime
        data['DAY_OF_WEEK'].fillna('Unknown', inplace=True)  # Fills all nan values in DAY_OF_WEEK column with 'Unknown'
        return data
    except:
        return "Error while loading file. Please ensure provided path to data file is correct and that the data is the correct dataset."  # Error to be returned if loading of data fails


def formatDate(dateString=''):
    '''Function for formatting a date provided by user input. Takes a string containing a date in format 'YYYY-MM-DD' as input parameter.
    Returns a date object in format YYYY-MM-DD'''
    tempString = dateString.strip().replace('-', ' ')
    tempString = tempString.split(' ')
    formattedDate = datetime.datetime(int(tempString[0]), int(tempString[1]), int(tempString[2]))
    return formattedDate


def searchPeriod(startDate, endDate, data):
    '''Function for filtering rows in a dataframe by a certain period. Takes two formatted dates (start and end date)
    as well as the dataframe (data) to be filtered as input parameters. Returns the resulting rows as a dataframe.'''
    mask = (data['ACCIDENT_DATE'] >= startDate) & (data['ACCIDENT_DATE'] <= endDate)
    results = data.loc[mask]
    return results


def searchStr(string, columnName, data):
    '''Function for filtering rows in a dataframe by a certain string/keyword. Search is not case sensitive and allows 
    a partial match. Takes the string to search for, the column in the dataframe to be searched (string) as well as the dataframe
    (data) to be filtered as input parameters. Returns the resulting rows as a dataframe.'''
    results = data[data[columnName].str.contains(string, case=False)]
    return results


def searchRange(minimum, maximum, columnName, data, flt=False):
    '''Function for filtering rows in a dataframe by a certain numeric range. Search is inclusive. Takes the minimum and maximum
    values, the column in the dataframe to be searched (string), the dataframe (data) to be filtered, as well as whether the 
    min and max values are floats (flt) as input parameters. Returns the resulting rows as a dataframe.'''
    if (re.search(r"\d*\.\d+", minimum) or re.search(r"\d+", minimum)) and (re.search(r"\d*\.\d+", maximum) or re.search(r"\d+", maximum)):
        if flt:
            minimum = float(minimum)
            maximum = float(maximum)
        else:
            minimum = int(minimum)
            maximum = int(maximum)
        mask = (data[columnName] >= minimum) & (data[columnName] <= maximum)
        results = data.loc[mask]
        return results
    else:
        return "Min and max values must be numeric." 


def getHourAvg(resultDf):
    '''Function for getting the average number of accidents per hour over a given period. Takes a dataframe as the input parameter
    and returns the results as a list.'''
    temp = pd.DataFrame({'COUNT' : resultDf.groupby(by=[resultDf.DATETIME.map(lambda x : x.hour)]).size()}).reset_index()  # Gets a dataframe where rows are grouped by hours
    temp.rename(columns={'DATETIME': 'HOUR'}, inplace=True)  # Renames DATETIME column to HOUR
    num_days = resultDf.groupby('ACCIDENT_DATE').size().count()  # Gets the total number of days of the period
    temp['AVG'] = temp['COUNT']/num_days  # Creates a new column containing the average number of accidents per hour
    result = pd.Series(temp['AVG'].values.round(2), index=temp['HOUR'])  # Converts desired columns to a pandas series
    return result


def valueCount(columnName, resultDf):
    '''Function for calculating the number of accidents per value in a specified column. Takes the name of the column to be
    analysed (string) as well as the dataframe containing that column as input parameters. Returns the results as a pandas series'''
    if columnName == 'SPEED_ZONE':  # Checks if the desired column if the 'SPEED_ZONE' column. Only used for ordering the results.
        temp = pd.DataFrame({'COUNT' : resultDf.groupby(by=[resultDf[columnName]]).size()}).reset_index()  # Creates a dataframe where rows are grouped by values in desired column
        temp['SPEED_NUMBER'] = [(re.sub('\D+', '', i)) for i in temp[columnName]]  # Creates a new temporary column containing only the digits in the speed zone value
        temp['SPEED_NUMBER'] = temp['SPEED_NUMBER'].replace(r'^\s*$', np.NaN, regex=True)  # Replaces empty string values in temporary column with NaN
        temp['SPEED_NUMBER'].fillna(0, inplace=True)  # Replaces NaN values with 0
        temp['SPEED_NUMBER'] = (temp['SPEED_NUMBER']).astype(int)  # Casts all values in temporary column to ints
        temp.sort_values('SPEED_NUMBER', inplace=True)  # Sorts results by temporary column
        result = pd.Series(temp['COUNT'].values, index=temp[columnName])  # Creates a pandas series from desired columns
    else:
        temp = pd.DataFrame({'COUNT' : resultDf.groupby(by=[resultDf[columnName]]).size()}).reset_index()  # Creates a dataframe where rows are grouped by values in desired column
        result = pd.Series(temp['COUNT'].values, index=temp[columnName])  # Creates a pandas series from desired columns
    return result


def plotSingleResult(result):
    '''Function for plotting a single series as a bar chart. Takes a pandas series as the input parameter and returns a
    matplotlib axes object.'''
    matplotlib.style.use('ggplot')
    ax = result.plot(kind='bar', grid=True, rot=80, xlabel=result.name, ylabel='Number of accidents', color='cornflowerblue')
    for bar in ax.patches:
        ax.annotate(str(bar.get_height()), (bar.get_x(), bar.get_height()))  # Adds bar values for each bar to the plot
    return ax


def displayResults(resultDf):
    '''Function for converting a dataframe into a list which may then be printed into a wx grid. Takes a pandas dataframe as the input 
    parameter.'''
    resultDf.fillna('None', inplace=True)
    resultDf.loc[:, 'ACCIDENT_DATE'] = resultDf.loc[:, 'ACCIDENT_DATE'].dt.strftime('%Y-%m-%d')
    result = resultDf.drop(['DATETIME'], axis=1)
    result = [result.columns.values.tolist()] + result.values.tolist()
    return result


def groupByAlcoholTime(resultDf):
    '''Function for grouping rows in a dataframe by ALCOHOLTIME and ACCIDENT_DATE. Takes a dataframe as the input parameter and
    returns two pandas series. One contains the counts per day where ALCOHOLTIME == No, and the other contains the counts per day
    where ALCOHOLTIME == Yes.'''
    temp = pd.DataFrame({'COUNT' : resultDf.groupby(by=[resultDf['ALCOHOLTIME'], resultDf['ACCIDENT_DATE']]).size()}).reset_index()
    alcNo = temp[temp['ALCOHOLTIME'] == 'No']
    alcNo = pd.Series(alcNo['COUNT'].values, index=alcNo['ACCIDENT_DATE']).rename('Alcoholtime_No')
    alcYes = temp[temp['ALCOHOLTIME'] == 'Yes']
    alcYes = pd.Series(alcYes['COUNT'].values, index=alcYes['ACCIDENT_DATE']).rename('Alcoholtime_Yes')
    return (alcNo, alcYes)


def plotMultipleResults(result1, result2):
    '''Function for plotting two pandas series as line graphs on the same plot. Takes two pandas series as the input parameters
    and returns a matplotlib axes object.'''
    matplotlib.style.use('ggplot')
    ax = result1.plot(kind='line', rot=80, xlabel='ACCIDENT_DATE', ylabel='Number of accidents', color='cornflowerblue', legend=result1.name)
    result2.plot(kind='line', rot=80, color='orangered', legend=result2.name, ax=ax)
    return ax

