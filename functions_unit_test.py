# Import necessary modules

import unittest
from unittest.mock import patch
from io import StringIO
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import datetime
from pandas.util.testing import assert_frame_equal
import functions


class TestLoadData(unittest.TestCase):
    '''
    Tests the loadData function from functions.py
    '''

    def test_loadData_empty_path(self):
        '''
        Tests if the function returns the string error message if an empty path is provided
        '''
        result = functions.loadData()
        expected_output = "Error while loading file. Please ensure provided path to data file is correct and that the data is the correct dataset."
        self.assertEqual(result, expected_output)

    def test_loadData_wrong_path(self):
        '''
        Tests if the function returns the string error message if an incorrect path is provided
        '''
        result = functions.loadData('wrong.csv')
        expected_output = "Error while loading file. Please ensure provided path to data file is correct and that the data is the correct dataset."
        self.assertEqual(result, expected_output)
        
    def test_loadData_correct_path(self):
        '''
        Tests if the function loads a dataframe if correct path is provided
        '''
        result = functions.loadData('Crash_Statistics_Victoria.csv')
        expected_output = pd.read_csv('Crash_Statistics_Victoria.csv')
        expected_output['ACCIDENT_DATE'] = pd.to_datetime(expected_output['ACCIDENT_DATE'])
        expected_output['DATETIME'] = pd.to_datetime(expected_output['ACCIDENT_DATE'].dt.strftime('%Y-%m-%d') + ' ' + expected_output['ACCIDENT_TIME'].str.replace('.', ':')) 
        assert_frame_equal(result, expected_output)


class TestFormatDate(unittest.TestCase):
    '''
    Tests the formatDate function from functions.py
    '''
        
    def test_formatDate_non_valid_date(self):
        result = functions.formatDate('2015-02-30')
        expected_output = 'Date is invalid/out of range.' 
        self.assertEqual(result, expected_output)

        
    def test_formatDate_correct_input(self):
        result = functions.formatDate('2015-03-15')
        expected_output = datetime.datetime(2015, 3, 15)
        self.assertEqual(result, expected_output)



class TestSearchPeriod(unittest.TestCase):
    '''
    Tests the searchPeriod function from functions.py
    '''

    def test_searchPeriod_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        start = functions.formatDate('2018-01-01')
        end = functions.formatDate('2018-01-03')
        mask = (data['ACCIDENT_DATE'] >= datetime.datetime(2018, 1, 1)) & (data['ACCIDENT_DATE'] <= datetime.datetime(2018, 1, 3))
        expected_output = data.loc[mask]
        result = functions.searchPeriod(start, end, data)
        assert_frame_equal(result, expected_output)
            
            
class TestSearchStr(unittest.TestCase):
    '''
    Tests the searchStr function from functions.py
    '''

    def test_searchStr_incorrect_colName(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        string = 'ped'
        colName = 'test'
        expected_output = "Column name is not valid."
        result = functions.searchStr(string, colName, data)
        self.assertEqual(result, expected_output)


    def test_searchStr_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        string = 'ped'
        colName = 'DCA_CODE'
        expected_output = data[data['DCA_CODE'].str.contains('ped', case=False)]
        result = functions.searchStr(string, colName, data)
        assert_frame_equal(result, expected_output)
                  
            
class TestSearchRange(unittest.TestCase):
    '''
    Tests the searchRange function from functions.py
    '''

    def test_searchRange_non_numeric_min(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        minimum = 'a'
        maximum = 4
        colName = 'NO_OF_VEHICLES'
        expected_output = "Min and max values must be numeric."
        result = functions.searchRange(minimum, maximum, colName, data)
        self.assertEqual(result, expected_output)
     
    
    def test_searchRange_non_numeric_max(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        minimum = 2
        maximum = 'b'
        colName = 'NO_OF_VEHICLES'
        expected_output = "Min and max values must be numeric."
        result = functions.searchRange(minimum, maximum, colName, data)
        self.assertEqual(result, expected_output)


    def test_searchRange_incorrect_colName(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        minimum = 3
        maximum = 5
        colName = 'test'
        expected_output = "Column name is not valid."
        result = functions.searchRange(minimum, maximum, colName, data)
        self.assertEqual(result, expected_output)


    def test_searchRange_correct_input_int(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        minimum = 3
        maximum = 5
        colName = 'NO_OF_VEHICLES'
        mask = (data['NO_OF_VEHICLES'] >= 3) & (data['NO_OF_VEHICLES'] <= 5)
        expected_output = data.loc[mask]
        result = functions.searchRange(minimum, maximum, colName, data)
        assert_frame_equal(result, expected_output)
        
    def test_searchRange_correct_input_float(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        minimum = 145.1
        maximum = 145.2
        colName = 'LONGITUDE'
        mask = (data['LONGITUDE'] >= 145.1) & (data['LONGITUDE'] <= 145.2)
        expected_output = data.loc[mask]
        result = functions.searchRange(minimum, maximum, colName, data, flt=True)
        assert_frame_equal(result, expected_output)

            
class TestGetHourAvg(unittest.TestCase):
    '''
    Tests the getHourAvg function from functions.py
    '''

    def test_getHourAvg_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        result = functions.getHourAvg(data)
        self.assertIsInstance(result, pd.Series)
           
            
class TestValueCount(unittest.TestCase):
    '''
    Tests the valueCount function from functions.py
    '''

    def test_valueCount_incorrect_colName(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        colName = 'test'
        expected_output = "Column name is not valid."
        result = functions.valueCount(colName, data)
        self.assertEqual(result, expected_output)


    def test_valueCount_correct_input_speed_zone(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        colName = 'SPEED_ZONE'
        result = functions.valueCount(colName, data)
        self.assertIsInstance(result, pd.Series)
        
    def test_valueCount_correct_input_other_col(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        colName = 'DAY_OF_WEEK'
        result = functions.valueCount(colName, data)
        self.assertIsInstance(result, pd.Series)
            
            
class TestPlotSingleResult(unittest.TestCase):
    '''
    Tests the plotSingleResult function from functions.py
    '''

    def test_plotSingleResult_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        test = functions.getHourAvg(data)
        result = functions.plotSingleResult(test)
        self.assertIsInstance(result, matplotlib.axes.Axes)
    
    
class TestDisplayResults(unittest.TestCase):
    '''
    Tests the displayResults function from functions.py
    '''

    def test_displayResults_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        test = functions.searchStr('T20130013611', 'ACCIDENT_NO', data)
        expected_output = [['OBJECTID', 'ACCIDENT_NO', 'ABS_CODE', 'ACCIDENT_STATUS', 'ACCIDENT_DATE', 'ACCIDENT_TIME', 'ALCOHOLTIME', 'ACCIDENT_TYPE', 'DAY_OF_WEEK', 'DCA_CODE', 'HIT_RUN_FLAG', 'LIGHT_CONDITION', 'POLICE_ATTEND', 'ROAD_GEOMETRY', 'SEVERITY', 'SPEED_ZONE', 'RUN_OFFROAD', 'NODE_ID', 'LONGITUDE', 'LATITUDE', 'NODE_TYPE', 'LGA_NAME', 'REGION_NAME', 'VICGRID_X', 'VICGRID_Y', 'TOTAL_PERSONS', 'INJ_OR_FATAL', 'FATALITY', 'SERIOUSINJURY', 'OTHERINJURY', 'NONINJURED', 'MALES', 'FEMALES', 'BICYCLIST', 'PASSENGER', 'DRIVER', 'PEDESTRIAN', 'PILLION', 'MOTORIST', 'UNKNOWN', 'PED_CYCLIST_5_12', 'PED_CYCLIST_13_18', 'OLD_PEDESTRIAN', 'OLD_DRIVER', 'YOUNG_DRIVER', 'ALCOHOL_RELATED', 'UNLICENCSED', 'NO_OF_VEHICLES', 'HEAVYVEHICLE', 'PASSENGERVEHICLE', 'MOTORCYCLE', 'PUBLICVEHICLE', 'DEG_URBAN_NAME', 'DEG_URBAN_ALL', 'LGA_NAME_ALL', 'REGION_NAME_ALL', 'SRNS', 'SRNS_ALL', 'RMA', 'RMA_ALL', 'DIVIDED', 'DIVIDED_ALL', 'STAT_DIV_NAME'], [3401771, 'T20130013611', 'ABS to receive accident', 'Finished', '2013-01-07', '06.40.00', 'No', 'Struck Pedestrian', 'Monday', 'PED PLAYING/LYING/WORKING/STANDING ON CARRIAGEWAY.', 'No', 'Dark Street lights on', 'Yes', 'T intersection', 'Other injury accident', '50 km/hr', 'No', 261607, 144.33957, -38.160990000000005, 'Intersection', 'GEELONG', 'SOUTH WESTERN REGION', 2442117.461, 2370953.264, 3, 1, 0, 0, 1, 2, 2, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'No', 0, 2, 0, 2, 0, 0, 'LARGE_PROVINCIAL_CITIES', 'LARGE_PROVINCIAL_CITIES', 'GEELONG', 'SOUTH WESTERN REGION', 'None', 'None', 'Local Road', 'Local Road', 'Undivided', 'Undiv', 'Country']]
        result = functions.displayResults(test)
        self.assertEqual(result, expected_output)
           
            
class TestGroupByAlcoholTime(unittest.TestCase):
    '''
    Tests the groupByAlcoholTime function from functions.py
    '''

    def test_groupByAlcoholTime_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        test = functions.searchPeriod('2015-03-16', '2015-03-23', data)
        result1, result2 = functions.groupByAlcoholTime(test)
        self.assertIsInstance(result1, pd.Series)
        self.assertIsInstance(result2, pd.Series)


class TestPlotMultipleResults(unittest.TestCase):
    '''
    Tests the plotMultipleResults function from functions.py
    '''

    def test_plotMultipleResults_correct_input(self):
        data = functions.loadData('Crash_Statistics_Victoria.csv')
        test = functions.searchPeriod('2015-03-16', '2015-03-23', data)
        no, yes = functions.groupByAlcoholTime(test)
        result = functions.plotMultipleResults(no, yes)
        self.assertIsInstance(result, matplotlib.axes.Axes)


if __name__ == '__main__':
    unittest.main()