proceed = True

try:
    import wx
    import wx.adv
    import wx.grid
    import matplotlib
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    import datetime
    import re
    import functions

except:
    proceed = False
    raise ImportError("Error while loading required modules. Program requires wx, matplotlib, numpy, pandas, datetime and re to run.")
    

class VicCrashAnal(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, size=(1000, 600))
        self.initialise()

    def initialise(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(380, 40), label='Victorian Crash Analyser')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.body = wx.Panel(self, pos=(200, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(420, 180), label='Select a tool to begin')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.toolsHead = wx.StaticText(self, pos=(430, 250), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(460, 280), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(430, 280), size=(20, 20))

        self.tool2 = wx.StaticText(self, pos=(460, 300), label='Average Accident Per Hour')
        self.tool2butt = wx.Button(self, pos=(430, 300), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(460, 320), label='Keyword Search')
        self.tool3butt = wx.Button(self, pos=(430, 320), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(460, 340), label='Alcohol Time Relationship')
        self.tool4butt = wx.Button(self, pos=(430, 340), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(460, 360), label='Speed Zone Accident Count')
        self.tool5butt = wx.Button(self, pos=(430, 360), size=(20, 20))


class PerAccInfo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(1000, 600))
        self.show()

    def show(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(360, 40), label='Time Period Accident Information')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.tools = wx.Panel(self, pos=(50, 150), size=(220, 150))
        self.tools.SetBackgroundColour((220, 220, 220))

        self.toolsHead = wx.StaticText(self, pos=(55, 160), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(85, 190), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(55, 190), size=(20, 20))
        self.tool1Sel = wx.StaticBox(self, pos=(48, 188), size=(225, 25))

        self.tool2 = wx.StaticText(self, pos=(85, 210), label='Average Accident Per Hour')
        self.tool2butt = wx.Button(self, pos=(55, 210), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(85, 230), label='Keyword Search')
        self.tool3butt = wx.Button(self, pos=(55, 230), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(85, 250), label='Alcohol Time Relationship')
        self.tool4butt = wx.Button(self, pos=(55, 250), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(85, 270), label='Speed Zone Accident Count')
        self.tool5butt = wx.Button(self, pos=(55, 270), size=(20, 20))

        self.body = wx.Panel(self, pos=(325, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(550, 180), label='Select time period')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.startHead = wx.StaticText(self, pos=(500, 250), label='Start date')
        self.startHead.SetForegroundColour((100, 100, 100))
        self.endHead = wx.StaticText(self, pos=(670, 250), label='End date')
        self.endHead.SetForegroundColour((100, 100, 100))
        fontDateHead = self.GetFont()
        fontDateHead.PointSize += 2
        self.startHead.SetFont(fontDateHead)
        self.endHead.SetFont(fontDateHead)

        self.startDate = wx.adv.DatePickerCtrl(self, pos=(490, 280))
        self.endDate = wx.adv.DatePickerCtrl(self, pos=(660, 280))

        self.searchButt = wx.Button(self, pos=(580, 350), size=(80, 20), label='Search')
        self.searchButt.Bind(wx.EVT_BUTTON, self.results)

    def results(self, event):
        if proceed:
            startDate = self.startDate.Value.FormatISODate()
            endDate = self.endDate.Value.FormatISODate()

            startDate = functions.formatDate(startDate)
            endDate = functions.formatDate(endDate)
            result = functions.searchPeriod(startDate, endDate, data)
            displayList = functions.displayResults(result)

            self.frame = ResultsFrame(displayList)


class AvgAccPerHour(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(1000, 600))
        self.show()

    def show(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(380, 40), label='Average Accident Per Hour')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.tools = wx.Panel(self, pos=(50, 150), size=(220, 150))
        self.tools.SetBackgroundColour((220, 220, 220))

        self.toolsHead = wx.StaticText(self, pos=(55, 160), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(85, 190), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(55, 190), size=(20, 20))

        self.tool2 = wx.StaticText(self, pos=(85, 210), label='Average Accident Per Hour')
        self.tool2Sel = wx.StaticBox(self, pos=(48, 208), size=(225, 25))
        self.tool2butt = wx.Button(self, pos=(55, 210), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(85, 230), label='Keyword Search')
        self.tool3butt = wx.Button(self, pos=(55, 230), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(85, 250), label='Alcohol Time Relationship')
        self.tool4butt = wx.Button(self, pos=(55, 250), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(85, 270), label='Speed Zone Accident Count')
        self.tool5butt = wx.Button(self, pos=(55, 270), size=(20, 20))

        self.body = wx.Panel(self, pos=(325, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(550, 180), label='Select time period')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.startHead = wx.StaticText(self, pos=(500, 250), label='Start date')
        self.startHead.SetForegroundColour((100, 100, 100))
        self.endHead = wx.StaticText(self, pos=(670, 250), label='End date')
        self.endHead.SetForegroundColour((100, 100, 100))
        fontDateHead = self.GetFont()
        fontDateHead.PointSize += 2
        self.startHead.SetFont(fontDateHead)
        self.endHead.SetFont(fontDateHead)

        self.startDate = wx.adv.DatePickerCtrl(self, pos=(490, 280))
        self.endDate = wx.adv.DatePickerCtrl(self, pos=(660, 280))

        self.searchButt = wx.Button(self, pos=(580, 350), size=(80, 20), label='Search')
        self.searchButt.Bind(wx.EVT_BUTTON, self.results)

    def results(self, event):
        if proceed:
            startDate = self.startDate.Value.FormatISODate()
            endDate = self.endDate.Value.FormatISODate()

            startDate = functions.formatDate(startDate)
            endDate = functions.formatDate(endDate)
            period = functions.searchPeriod(startDate, endDate, data)
            result = functions.getHourAvg(period)

            if len(result) > 1:
                plt.figure(figsize=(8, 6), num='Results')
                functions.plotSingleResult(result)
                plt.show()

            else:
                self.frame = ResultsFrame(result)


class KeySearch(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(1000, 600))
        self.show()

    def show(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(430, 40), label='Keyword Search')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.tools = wx.Panel(self, pos=(50, 150), size=(220, 150))
        self.tools.SetBackgroundColour((220, 220, 220))

        self.toolsHead = wx.StaticText(self, pos=(55, 160), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(85, 190), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(55, 190), size=(20, 20))

        self.tool2 = wx.StaticText(self, pos=(85, 210), label='Average Accident Per Hour')
        self.tool2butt = wx.Button(self, pos=(55, 210), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(85, 230), label='Keyword Search')
        self.tool3Sel = wx.StaticBox(self, pos=(48, 228), size=(225, 25))
        self.tool3butt = wx.Button(self, pos=(55, 230), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(85, 250), label='Alcohol Time Relationship')
        self.tool4butt = wx.Button(self, pos=(55, 250), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(85, 270), label='Speed Zone Accident Count')
        self.tool5butt = wx.Button(self, pos=(55, 270), size=(20, 20))

        self.body = wx.Panel(self, pos=(325, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(500, 180), label='Enter keyword/s to search dataset')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.subHead = wx.StaticText(self, pos=(490, 200), label='Put "," when searching multiple keywords')
        self.subHead.SetForegroundColour((100, 100, 100))
        fontSubHead = self.GetFont()
        fontSubHead.PointSize += 2
        self.subHead.SetFont(fontSubHead)

        self.keySearch = wx.SearchCtrl(self, pos=(520, 280), size=(200, 20))
        self.keySearch.ShowCancelButton(True)

        self.searchButt = wx.Button(self, pos=(580, 350), size=(80, 20), label='Search')
        self.searchButt.Bind(wx.EVT_BUTTON, self.results)

    def results(self, event):
        if proceed:
            string = self.keySearch.Value
            colName = 'DCA_CODE'

            result = functions.searchStr(string, colName, data)
            displayList = functions.displayResults(result)

            self.frame = ResultsFrame(displayList)


class AlcTimeRel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(1000, 600))
        self.show()

    def show(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(390, 40), label='Alcohol Time Relationship')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.tools = wx.Panel(self, pos=(50, 150), size=(220, 150))
        self.tools.SetBackgroundColour((220, 220, 220))

        self.toolsHead = wx.StaticText(self, pos=(55, 160), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(85, 190), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(55, 190), size=(20, 20))

        self.tool2 = wx.StaticText(self, pos=(85, 210), label='Average Accident Per Hour')
        self.tool2butt = wx.Button(self, pos=(55, 210), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(85, 230), label='Keyword Search')
        self.tool3butt = wx.Button(self, pos=(55, 230), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(85, 250), label='Alcohol Time Relationship')
        self.tool4Sel = wx.StaticBox(self, pos=(48, 248), size=(225, 25))
        self.tool4butt = wx.Button(self, pos=(55, 250), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(85, 270), label='Speed Zone Accident Count')
        self.tool5butt = wx.Button(self, pos=(55, 270), size=(20, 20))

        self.body = wx.Panel(self, pos=(325, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(460, 180), label='Select a variable from the dataset to compare')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.startHead = wx.StaticText(self, pos=(500, 250), label='Start date')
        self.startHead.SetForegroundColour((100, 100, 100))
        self.endHead = wx.StaticText(self, pos=(670, 250), label='End date')
        self.endHead.SetForegroundColour((100, 100, 100))
        fontDateHead = self.GetFont()
        fontDateHead.PointSize += 2
        self.startHead.SetFont(fontDateHead)
        self.endHead.SetFont(fontDateHead)

        self.startDate = wx.adv.DatePickerCtrl(self, pos=(490, 280))
        self.endDate = wx.adv.DatePickerCtrl(self, pos=(660, 280))

        self.colHead = wx.StaticText(self, pos=(360, 350), label='Select column')
        self.column = wx.ComboBox(self, pos=(360, 380), choices=['DAY_OF_WEEK', 'DCA_CODE', 'LIGHT_CONDITION', 'NO_OF_VEHICLES', 'LONGITUDE', 'LATITUDE'])
        self.column.Bind(wx.EVT_COMBOBOX, self.var_sel)

        self.additionalSearch = self.column.GetCurrentSelection()

        self.varColHead = wx.StaticText(self, pos=(580, 350), label='Specify column criteria')
        self.varStr = wx.TextCtrl(self, pos=(580, 380), size=(150, 20))

        self.varMinHead = wx.StaticText(self, pos=(580, 330), label='Specify minimum value')
        self.varMinHead.Hide()
        self.varMin = wx.TextCtrl(self, pos=(580, 360), size=(150, 20))
        self.varMin.Hide()
        self.varMaxHead = wx.StaticText(self, pos=(580, 390), label='Specify maximum value')
        self.varMaxHead.Hide()
        self.varMax = wx.TextCtrl(self, pos=(580, 420), size=(150, 20))
        self.varMax.Hide()

        fontSelHead = self.GetFont()
        fontSelHead = fontSelHead.Bold()
        self.colHead.SetFont(fontSelHead)
        self.varColHead.SetFont(fontSelHead)
        self.varMinHead.SetFont(fontSelHead)
        self.varMaxHead.SetFont(fontSelHead)

        self.searchButt = wx.Button(self, pos=(800, 380), size=(80, 20), label='Search')
        self.searchButt.Bind(wx.EVT_BUTTON, self.results)

    def var_sel(self, event):
        self.additionalSearch = self.column.GetCurrentSelection()

        if self.additionalSearch <= 2:
            self.varColHead.Show()
            self.varStr.Show()
            self.varMinHead.Hide()
            self.varMin.Hide()
            self.varMaxHead.Hide()
            self.varMax.Hide()

        elif self.additionalSearch >= 3:
            self.varColHead.Hide()
            self.varStr.Hide()
            self.varMinHead.Show()
            self.varMin.Show()
            self.varMaxHead.Show()
            self.varMax.Show()

    def results(self, event):
        if proceed:
            startDate = self.startDate.Value.FormatISODate()
            endDate = self.endDate.Value.FormatISODate()

            startDate = functions.formatDate(startDate)
            endDate = functions.formatDate(endDate)

            period = functions.searchPeriod(startDate, endDate, data)

            if self.additionalSearch == 0:
                string = self.varStr.Value
                colName = 'DAY_OF_WEEK'
                period = functions.searchStr(string, colName, period)

            elif self.additionalSearch == 1:
                string = self.varStr.Value
                colName = 'DCA_CODE'
                period = functions.searchStr(string, colName, period)

            elif self.additionalSearch == 2:
                string = self.varStr.Value
                colName = 'LIGHT_CONDITION'
                period = functions.searchStr(string, colName, period)

            elif self.additionalSearch == 3:
                minimum = self.varMin.Value
                maximum = self.varMax.Value
                colName = 'NO_OF_VEHICLES'
                flt = False
                period = functions.searchRange(minimum, maximum, colName, period, flt)
                
            elif self.additionalSearch == 4:
                minimum = self.varMin.Value
                maximum = self.varMax.Value
                colName = 'LONGITUDE'
                flt = True
                period = functions.searchRange(minimum, maximum, colName, period, flt)
                
            elif self.additionalSearch == 5:
                minimum = self.varMin.Value
                maximum = self.varMax.Value
                colName = 'LATITUDE'
                flt = True
                period = functions.searchRange(minimum, maximum, colName, period, flt)
                print(period)

            if isinstance(period, str):
                wx.MessageBox(period)

            else:
                result1, result2 = functions.groupByAlcoholTime(period)

                if len(result1) > 1 and len(result2) > 1:
                    plt.figure(figsize=(8, 6), num='Results')
                    functions.plotMultipleResults(result1, result2)
                    plt.show()

                else:
                    self.frame = ResultsFrame(result1)


class SpdZneAccCnt(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(1000, 600))
        self.show()

    def show(self):
        self.header = wx.Panel(self, size=(1000, 100))
        self.header.SetBackgroundColour((220, 220, 220))

        self.title = wx.StaticText(self, pos=(380, 40), label='Speed Zone Accident Count')
        self.title.SetForegroundColour((100, 100, 100))
        fontTitle = self.GetFont()
        fontTitle.PointSize += 8
        self.title.SetFont(fontTitle)

        self.tools = wx.Panel(self, pos=(50, 150), size=(220, 150))
        self.tools.SetBackgroundColour((220, 220, 220))

        self.toolsHead = wx.StaticText(self, pos=(55, 160), label='Tools')
        fontToolsHead = self.GetFont()
        fontToolsHead = fontToolsHead.Bold()
        self.toolsHead.SetFont(fontToolsHead)

        self.tool1 = wx.StaticText(self, pos=(85, 190), label='Period Accident Information')
        self.tool1butt = wx.Button(self, pos=(55, 190), size=(20, 20))

        self.tool2 = wx.StaticText(self, pos=(85, 210), label='Average Accident Per Hour')
        self.tool2butt = wx.Button(self, pos=(55, 210), size=(20, 20))

        self.tool3 = wx.StaticText(self, pos=(85, 230), label='Keyword Search')
        self.tool3butt = wx.Button(self, pos=(55, 230), size=(20, 20))

        self.tool4 = wx.StaticText(self, pos=(85, 250), label='Alcohol Time Relationship')
        self.tool4butt = wx.Button(self, pos=(55, 250), size=(20, 20))

        self.tool5 = wx.StaticText(self, pos=(85, 270), label='Speed Zone Accident Count')
        self.tool5Sel = wx.StaticBox(self, pos=(48, 268), size=(225, 25))
        self.tool5butt = wx.Button(self, pos=(55, 270), size=(20, 20))

        self.body = wx.Panel(self, pos=(325, 150), size=(600, 300))
        self.body.SetBackgroundColour((220, 220, 220))

        self.head = wx.StaticText(self, pos=(550, 180), label='Select time period')
        self.head.SetForegroundColour((100, 100, 100))
        fontHead = self.GetFont()
        fontHead.PointSize += 4
        self.head.SetFont(fontHead)

        self.startHead = wx.StaticText(self, pos=(500, 250), label='Start date')
        self.startHead.SetForegroundColour((100, 100, 100))
        self.endHead = wx.StaticText(self, pos=(670, 250), label='End date')
        self.endHead.SetForegroundColour((100, 100, 100))
        fontDateHead = self.GetFont()
        fontDateHead.PointSize += 2
        self.startHead.SetFont(fontDateHead)
        self.endHead.SetFont(fontDateHead)

        self.startDate = wx.adv.DatePickerCtrl(self, pos=(490, 280))
        self.endDate = wx.adv.DatePickerCtrl(self, pos=(660, 280))

        self.searchButt = wx.Button(self, pos=(580, 350), size=(80, 20), label='Search')
        self.searchButt.Bind(wx.EVT_BUTTON, self.results)

    def results(self, event):
            if proceed:
                startDate = self.startDate.Value.FormatISODate()
                endDate = self.endDate.Value.FormatISODate()

                startDate = functions.formatDate(startDate)
                endDate = functions.formatDate(endDate)
                period = functions.searchPeriod(startDate, endDate, data)
                result = functions.valueCount('SPEED_ZONE', period)

                if len(result) > 1:
                    plt.figure(figsize=(8, 6), num='Results')
                    plt.subplots_adjust(bottom=0.4)
                    functions.plotSingleResult(result)
                    plt.show()

                else:
                    self.frame = ResultsFrame(result)


class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, title='Victorian Crash Analyser', size=(1000, 600))

        self.mainPanel = VicCrashAnal(self)
        self.mainPanel.tool1butt.Bind(wx.EVT_BUTTON, self.show_per_acc_info)
        self.mainPanel.tool2butt.Bind(wx.EVT_BUTTON, self.show_avg_acc_per_hr)
        self.mainPanel.tool3butt.Bind(wx.EVT_BUTTON, self.show_key_srch)
        self.mainPanel.tool4butt.Bind(wx.EVT_BUTTON, self.show_alc_tme_rel)
        self.mainPanel.tool5butt.Bind(wx.EVT_BUTTON, self.show_spd_zne_acc_cnt)

        self.panel1 = PerAccInfo(self)
        self.panel1.tool2butt.Bind(wx.EVT_BUTTON, self.show_avg_acc_per_hr)
        self.panel1.tool3butt.Bind(wx.EVT_BUTTON, self.show_key_srch)
        self.panel1.tool4butt.Bind(wx.EVT_BUTTON, self.show_alc_tme_rel)
        self.panel1.tool5butt.Bind(wx.EVT_BUTTON, self.show_spd_zne_acc_cnt)
        self.panel1.Hide()

        self.panel2 = AvgAccPerHour(self)
        self.panel2.tool1butt.Bind(wx.EVT_BUTTON, self.show_per_acc_info)
        self.panel2.tool3butt.Bind(wx.EVT_BUTTON, self.show_key_srch)
        self.panel2.tool4butt.Bind(wx.EVT_BUTTON, self.show_alc_tme_rel)
        self.panel2.tool5butt.Bind(wx.EVT_BUTTON, self.show_spd_zne_acc_cnt)
        self.panel2.Hide()

        self.panel3 = KeySearch(self)
        self.panel3.tool1butt.Bind(wx.EVT_BUTTON, self.show_per_acc_info)
        self.panel3.tool2butt.Bind(wx.EVT_BUTTON, self.show_avg_acc_per_hr)
        self.panel3.tool4butt.Bind(wx.EVT_BUTTON, self.show_alc_tme_rel)
        self.panel3.tool5butt.Bind(wx.EVT_BUTTON, self.show_spd_zne_acc_cnt)
        self.panel3.Hide()

        self.panel4 = AlcTimeRel(self)
        self.panel4.tool1butt.Bind(wx.EVT_BUTTON, self.show_per_acc_info)
        self.panel4.tool2butt.Bind(wx.EVT_BUTTON, self.show_avg_acc_per_hr)
        self.panel4.tool3butt.Bind(wx.EVT_BUTTON, self.show_key_srch)
        self.panel4.tool5butt.Bind(wx.EVT_BUTTON, self.show_spd_zne_acc_cnt)
        self.panel4.Hide()

        self.panel5 = SpdZneAccCnt(self)
        self.panel5.tool1butt.Bind(wx.EVT_BUTTON, self.show_per_acc_info)
        self.panel5.tool2butt.Bind(wx.EVT_BUTTON, self.show_avg_acc_per_hr)
        self.panel5.tool3butt.Bind(wx.EVT_BUTTON, self.show_key_srch)
        self.panel5.tool4butt.Bind(wx.EVT_BUTTON, self.show_alc_tme_rel)
        self.panel5.Hide()

    def show_per_acc_info(self, event):
        self.mainPanel.Hide()
        self.panel1.Show()
        self.panel2.Hide()
        self.panel3.Hide()
        self.panel4.Hide()
        self.panel5.Hide()

    def show_avg_acc_per_hr(self, event):
        self.mainPanel.Hide()
        self.panel1.Hide()
        self.panel2.Show()
        self.panel3.Hide()
        self.panel4.Hide()
        self.panel5.Hide()

    def show_key_srch(self, event):
        self.mainPanel.Hide()
        self.panel1.Hide()
        self.panel2.Hide()
        self.panel3.Show()
        self.panel4.Hide()
        self.panel5.Hide()

    def show_alc_tme_rel(self, event):
        self.mainPanel.Hide()
        self.panel1.Hide()
        self.panel2.Hide()
        self.panel3.Hide()
        self.panel4.Show()
        self.panel5.Hide()

    def show_spd_zne_acc_cnt(self, event):
        self.mainPanel.Hide()
        self.panel1.Hide()
        self.panel2.Hide()
        self.panel3.Hide()
        self.panel4.Hide()
        self.panel5.Show()

    def show_error(self):
        self.bar = self.CreateStatusBar()
        self.bar.SetStatusText(data)


class ResultsFrame(wx.Frame):
    def __init__(self, displayList, title='Results', parent=None):
        wx.Frame.__init__(self, parent=parent, title=title, size=(800, 600))

        if len(displayList) > 1:
            self.grid = wx.grid.Grid(self, -1)
            self.grid.CreateGrid(len(displayList), len(displayList[0]))
            self.grid.SetRowSize(0, 60)
            self.grid.SetColSize(0, 120)

            for row in range(len(displayList)):
                for col in range(len(displayList[0])):
                    self.grid.SetCellValue(row, col, str(displayList[row][col]))
                    self.grid.SetReadOnly(row, col)

        else:
            self.noResults = wx.StaticText(self, label="No results found")

        self.bar = self.CreateStatusBar()

        if len(displayList) < 1:
            self.bar.SetStatusText(str(len(displayList)) + " results found")

        else:
            self.bar.SetStatusText(str(len(displayList) - 1) + " results found")

        self.Show()


if __name__ == '__main__':
    app = wx.App()
    frame = MainFrame()
    frame.Show()

    data = functions.loadData("Crash_Statistics_Victoria.csv")
    if type(data) == str:
        proceed = False
        wx.MessageBox(data)
        frame.show_error()

    app.MainLoop()